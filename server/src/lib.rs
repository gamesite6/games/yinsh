use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::{HashMap, HashSet},
    iter::FromIterator,
    ops::AddAssign,
};
use utils::get_valid_moves;

mod utils;

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum Color {
    #[serde(rename = "w")]
    White,
    #[serde(rename = "b")]
    Black,
}

impl Color {
    fn flip(&mut self) {
        match self {
            Self::White => *self = Self::Black,
            Self::Black => *self = Self::White,
        }
    }
    fn flipped(&self) -> Color {
        match self {
            Self::White => Self::Black,
            Self::Black => Self::White,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Placements(HashMap<CoordX, HashMap<CoordY, Color>>);

impl Placements {
    fn new() -> Self {
        Placements(HashMap::new())
    }
    fn get(&self, coord: Coord) -> Option<Color> {
        let Coord(x, y) = coord;
        self.0.get(&x)?.get(&y).copied()
    }
    pub fn remove(&mut self, coord: Coord) -> Option<Color> {
        let Coord(x, y) = coord;
        let ys = self.0.get_mut(&x)?;
        let removed = ys.remove(&y);
        if ys.is_empty() {
            self.0.remove(&x);
        }
        removed
    }

    fn coords_colored(&self, color: Color) -> Vec<Coord> {
        self.0
            .iter()
            .flat_map(|(&x, ys)| {
                ys.iter()
                    .filter(|(_, &c)| c == color)
                    .map(move |(&y, _)| Coord(x, y))
            })
            .collect::<Vec<_>>()
    }
    fn find_5_in_row_from_coord(&self, start: Coord) -> Vec<HashSet<Coord>> {
        let color = match self.get(start) {
            Some(color) => color,
            None => return vec![],
        };

        let mut result = vec![];

        let directions = [utils::NORTH, utils::NORTH_EAST, utils::SOUTH_EAST];

        for &step in directions.iter() {
            if Coord::iter(start + step, step, 4).all(|c| self.get(c) == Some(color)) {
                let row = Coord::iter(start, step, 5).collect::<HashSet<_>>();
                result.push(row);
            }
        }

        result
    }

    fn has_5_in_a_row_colored(&self, color: Color) -> bool {
        self.coords_colored(color)
            .iter()
            .any(|&c| !self.find_5_in_row_from_coord(c).is_empty())
    }

    fn find_5_in_a_row_colored(&self, color: Color) -> Vec<HashSet<Coord>> {
        let mut result = vec![];
        for &c in self.coords_colored(color).iter() {
            for row in self.find_5_in_row_from_coord(c) {
                result.push(row);
            }
        }
        result
    }

    fn get_mut(&mut self, coord: Coord) -> Option<&mut Color> {
        let Coord(x, y) = coord;
        self.0.get_mut(&x)?.get_mut(&y)
    }
    fn insert(&mut self, coord: Coord, color: Color) -> Option<Color> {
        let Coord(x, y) = coord;
        let ys = self.0.entry(x).or_insert_with(HashMap::new);
        ys.insert(y, color)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Coord(CoordX, CoordY);

impl Coord {
    pub fn new(x: i8, y: i8) -> Self {
        Coord(CoordX(x), CoordY(y))
    }

    fn iter(start: Coord, step: Coord, count: usize) -> impl Iterator<Item = Coord> {
        CoordIterator { start, step, count }
    }

    fn coords_between(&self, to: &Coord) -> Option<Vec<Coord>> {
        let Coord(from_x, from_y) = self;
        let Coord(to_x, to_y) = to;

        let direction = Coord(
            CoordX((to_x.0 - from_x.0).signum()),
            CoordY((to_y.0 - from_y.0).signum()),
        );

        let mut coord = *self + direction;
        let mut result = vec![];

        while coord.0 <= CoordX::MAX
            && coord.0 >= CoordX::MIN
            && coord.1 <= CoordY::MAX
            && coord.1 >= CoordY::MIN
        {
            if coord == *to {
                return Some(result);
            } else {
                result.push(coord);
                coord = coord + direction;
            }
        }

        None
    }
}

struct CoordIterator {
    start: Coord,
    step: Coord,
    count: usize,
}

impl Iterator for CoordIterator {
    type Item = Coord;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count > 0 {
            self.count -= 1;
            let coord = self.start;
            self.start = self.start + self.step;
            Some(coord)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod coord_iterator_test {
    use super::*;

    #[test]
    fn test_iterator() {
        let iter = Coord::iter(Coord::new(0, 0), Coord::new(1, 1), 4);
        let coords = iter.collect::<Vec<_>>();
        assert_eq!(
            coords,
            vec![
                Coord::new(0, 0),
                Coord::new(1, 1),
                Coord::new(2, 2),
                Coord::new(3, 3)
            ]
        )
    }
}

impl std::ops::Add for Coord {
    type Output = Coord;

    fn add(self, rhs: Self) -> Self::Output {
        let Coord(CoordX(x), CoordY(y)) = self;
        let Coord(CoordX(other_x), CoordY(other_y)) = rhs;

        Coord(CoordX(x + other_x), CoordY(y + other_y))
    }
}

impl std::ops::Mul<i8> for Coord {
    type Output = Coord;

    fn mul(self, rhs: i8) -> Self::Output {
        let Coord(CoordX(x), CoordY(y)) = self;

        Coord(CoordX(x * rhs), CoordY(y * rhs))
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct CoordX(i8);

impl CoordX {
    pub const MAX: CoordX = CoordX(-1);
    pub const MIN: CoordX = CoordX(-11);

    pub const A: CoordX = CoordX(-11);
    pub const B: CoordX = CoordX(-10);
    pub const C: CoordX = CoordX(-9);
    pub const D: CoordX = CoordX(-8);
    pub const E: CoordX = CoordX(-7);
    pub const F: CoordX = CoordX(-6);
    pub const G: CoordX = CoordX(-5);
    pub const H: CoordX = CoordX(-4);
    pub const I: CoordX = CoordX(-3);
    pub const J: CoordX = CoordX(-2);
    pub const K: CoordX = CoordX(-1);
}

impl AddAssign for CoordX {
    fn add_assign(&mut self, rhs: CoordX) {
        self.0 += rhs.0
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct CoordY(i8);

impl CoordY {
    pub const MAX: CoordY = CoordY(11);
    pub const MIN: CoordY = CoordY(1);
}

impl AddAssign for CoordY {
    fn add_assign(&mut self, rhs: CoordY) {
        self.0 += rhs.0
    }
}

pub type PlayerId = i32;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Settings {
    first_to_n_rings: u8,
}

impl Settings {
    pub fn validate(&self) -> Result<Vec<usize>, String> {
        let valid_n: HashSet<u8> = HashSet::from_iter([1, 3]);
        if valid_n.contains(&self.first_to_n_rings) {
            Ok(vec![2])
        } else {
            Err(String::from("n (rings) must be 1 or 3"))
        }
    }
}

#[cfg(test)]
mod settings_test {

    use super::*;
    use serde_json::*;

    #[test]
    fn deserialize_settings() {
        let settings: Settings = serde_json::from_value(json!({ "firstToNRings": 3 })).unwrap();
        assert_eq!(
            settings,
            Settings {
                first_to_n_rings: 3
            }
        )
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GameState {
    markers: Placements,
    players: Players,
    phase: Phase,
}

impl GameState {
    pub fn rings(&self) -> Placements {
        let mut placements = Placements::new();
        for &ring in &self.players.black.rings {
            if let Some(coord) = ring {
                placements.insert(coord, Color::Black);
            }
        }
        for &ring in &self.players.white.rings {
            if let Some(coord) = ring {
                placements.insert(coord, Color::White);
            }
        }
        placements
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerState {
    id: PlayerId,
    rings: [Option<Coord>; 5],
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Players {
    black: PlayerState,
    white: PlayerState,
}

impl Players {
    fn get_player(&self, color: Color) -> &PlayerState {
        match color {
            Color::Black => &self.black,
            Color::White => &self.white,
        }
    }
    fn get_player_mut(&mut self, color: Color) -> &mut PlayerState {
        match color {
            Color::Black => &mut self.black,
            Color::White => &mut self.white,
        }
    }
    fn find_color(&self, player_id: PlayerId) -> Option<Color> {
        if self.black.id == player_id {
            Some(Color::Black)
        } else if self.white.id == player_id {
            Some(Color::White)
        } else {
            None
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "t")]
pub enum Phase {
    RingPlacing { color: Color },
    MarkerPlacing { color: Color },
    RingMoving { color: Color, coord: Coord },
    MarkerTaking { color: Color },
    RingTaking { color: Color },

    GameComplete,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "t")]
pub enum Action {
    PlaceRing { coord: Coord },
    PlaceMarker { coord: Coord },
    MoveRing { coord: Coord },
    TakeMarkers { coord: Coord },
    TakeRing { coord: Coord },
}

#[cfg(test)]
mod action_test {
    use super::*;
    use serde_json::*;

    #[test]
    fn serialize_action_json() {
        let action = Action::PlaceRing {
            coord: Coord::new(-5, 6),
        };
        let value = serde_json::to_value(action).unwrap();
        assert_eq!(value, json!({ "t": "PlaceRing", "coord": [-5, 6] }))
    }

    #[test]
    fn deserialize_action_json() {
        let value = json!({ "t": "PlaceRing", "coord": [-1, 7] });

        let action = serde_json::from_value::<Action>(value).unwrap();
        assert_eq!(
            action,
            Action::PlaceRing {
                coord: Coord::new(-1, 7)
            }
        )
    }
}

pub fn initial_state(
    players_ids: &HashSet<PlayerId>,
    _settings: &Settings,
    seed: i64,
) -> Option<GameState> {
    if players_ids.len() != 2 {
        return None;
    }

    let mut rng = rand_pcg::Pcg32::from_seed(i128::from(seed).to_le_bytes());

    let mut players = players_ids
        .iter()
        .map(|&id| PlayerState {
            id,
            rings: [None; 5],
        })
        .collect::<Vec<_>>();

    players.shuffle(&mut rng);

    let white_player = players.pop()?;
    let black_player = players.pop()?;

    Some(GameState {
        players: Players {
            white: white_player,
            black: black_player,
        },
        markers: Placements::new(),
        phase: Phase::RingPlacing {
            color: Color::White,
        },
    })
}

pub fn perform_action(
    previous_state: &GameState,
    action: &Action,
    settings: &Settings,
    performed_by: PlayerId,
) -> Option<GameState> {
    let performed_by: Color = previous_state.players.find_color(performed_by)?;
    if !action_is_allowed(action, previous_state, performed_by) {
        log::debug!("not allowed!");
        return None;
    }

    let mut state = previous_state.clone();

    match &previous_state.phase {
        &Phase::RingPlacing { color } => match action {
            Action::PlaceRing { coord } => {
                let player = state.players.get_player_mut(color);
                let ring = player.rings.iter_mut().find(|r| r.is_none())?;
                ring.replace(*coord);

                if color == Color::Black && player.rings.iter().all(|r| r.is_some()) {
                    state.phase = Phase::MarkerPlacing {
                        color: Color::White,
                    }
                } else {
                    state.phase = Phase::RingPlacing {
                        color: color.flipped(),
                    }
                }
            }
            _ => {}
        },
        Phase::MarkerPlacing { color } => match action {
            Action::PlaceMarker { coord } => {
                state.markers.insert(*coord, *color);
                state.phase = Phase::RingMoving {
                    color: *color,
                    coord: *coord,
                };
            }
            _ => {}
        },
        &Phase::RingMoving { color, coord: from } => match action {
            Action::MoveRing { coord: to } => {
                let player = state.players.get_player_mut(color);
                let ring = player
                    .rings
                    .iter_mut()
                    .find(|r| **r == Some(from))?
                    .as_mut()?;

                for coord in from.coords_between(to)? {
                    if let Some(marker) = state.markers.get_mut(coord) {
                        marker.flip()
                    }
                }

                *ring = *to;

                if state.markers.has_5_in_a_row_colored(color) {
                    state.phase = Phase::MarkerTaking { color }
                } else {
                    let opponent_color = color.flipped();
                    if state.markers.has_5_in_a_row_colored(opponent_color) {
                        state.phase = Phase::MarkerTaking {
                            color: opponent_color,
                        }
                    } else {
                        state.phase = Phase::MarkerPlacing {
                            color: opponent_color,
                        };
                    }
                }
            }
            _ => {}
        },
        &Phase::MarkerTaking { color } => match action {
            &Action::TakeMarkers { coord } => {
                let rows = state.markers.find_5_in_a_row_colored(color);
                let row = rows.iter().find(|r| r.contains(&coord))?;

                for &c in row {
                    state.markers.remove(c);
                }

                state.phase = Phase::RingTaking { color };
            }
            _ => {}
        },

        &Phase::RingTaking { color } => match action {
            &Action::TakeRing { coord } => {
                let player = state.players.get_player_mut(color);
                let ring = player.rings.iter_mut().find(|c| **c == Some(coord))?;
                let _ = ring.take();

                if player.rings.iter().filter(|r| r.is_none()).count()
                    == usize::from(settings.first_to_n_rings)
                {
                    state.phase = Phase::GameComplete;
                } else if state.markers.has_5_in_a_row_colored(color) {
                    state.phase = Phase::MarkerTaking { color }
                } else {
                    let opponent_color = color.flipped();
                    if state.markers.has_5_in_a_row_colored(opponent_color) {
                        state.phase = Phase::MarkerTaking {
                            color: opponent_color,
                        }
                    } else {
                        state.phase = Phase::MarkerPlacing {
                            color: opponent_color,
                        };
                    }
                }
            }
            _ => {}
        },

        Phase::GameComplete => {}
    }

    Some(state)
}

fn action_is_allowed(action: &Action, state: &GameState, performed_by: Color) -> bool {
    match &state.phase {
        &Phase::RingPlacing { color } => match action {
            &Action::PlaceRing { coord } => {
                let acting_in_turn = color == performed_by;

                let occupied_coords = Iterator::chain(
                    state.players.black.rings.iter(),
                    state.players.white.rings.iter(),
                )
                .filter_map(|c| *c)
                .collect::<HashSet<_>>();

                acting_in_turn && !occupied_coords.contains(&coord)
            }
            _ => false,
        },
        &Phase::MarkerPlacing { color } => match action {
            &Action::PlaceMarker { coord } => {
                let acting_in_turn = color == performed_by;
                let has_ring_at_coord = state
                    .players
                    .get_player(color)
                    .rings
                    .iter()
                    .any(|&c| c == Some(coord));

                let has_valid_moves =
                    !get_valid_moves(&state.rings(), &state.markers, coord).is_empty();

                acting_in_turn && has_ring_at_coord && has_valid_moves
            }
            _ => false,
        },
        &Phase::RingMoving { color, .. } => match action {
            Action::MoveRing { .. } => color == performed_by,
            _ => false,
        },
        &Phase::MarkerTaking { color } => match action {
            Action::TakeMarkers { .. } => color == performed_by,
            _ => false,
        },
        &Phase::RingTaking { color } => match action {
            Action::TakeRing { .. } => color == performed_by,
            _ => false,
        },
        Phase::GameComplete => false,
    }
}

pub fn is_game_complete(state: &GameState) -> bool {
    matches!(state.phase, Phase::GameComplete { .. })
}
