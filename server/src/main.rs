use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};
use ringmaster_server::*;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::env;

#[post("/info")]
async fn info_endpoint(req: web::Json<InfoReq>) -> impl Responder {
    match req.settings.validate() {
        Ok(player_counts) => HttpResponse::Ok().json(InfoRes { player_counts }),
        Err(str) => HttpResponse::UnprocessableEntity().body(str),
    }
}

#[post("/initial-state")]
async fn initial_state_endpoint(req: web::Json<InitialStateReq>) -> impl Responder {
    match initial_state(&req.players, &req.settings, req.seed) {
        Some(state) => HttpResponse::Ok().json(InitialStateRes { state }),
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[post("/perform-action")]
async fn perform_action_endpoint(req: web::Json<PerformActionReq>) -> impl Responder {
    match perform_action(&req.state, &req.action, &req.settings, req.performed_by) {
        Some(next_state) => HttpResponse::Ok().json(PerformActionRes {
            completed: is_game_complete(&next_state),
            next_state,
        }),
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let port: u16 = env::var("PORT").unwrap().parse().unwrap();
    let addr = format!("0.0.0.0:{}", port);

    println!("Listening on {}", addr);

    HttpServer::new(|| {
        App::new()
            .service(info_endpoint)
            .service(initial_state_endpoint)
            .service(perform_action_endpoint)
    })
    .bind(addr)?
    .run()
    .await
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoReq {
    pub settings: Settings,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoRes {
    pub player_counts: Vec<usize>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateReq {
    pub players: HashSet<PlayerId>,
    pub settings: Settings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateRes {
    pub state: GameState,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionReq {
    pub performed_by: PlayerId,
    pub action: Action,
    pub state: GameState,
    pub settings: Settings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionRes {
    pub completed: bool,
    pub next_state: GameState,
}
