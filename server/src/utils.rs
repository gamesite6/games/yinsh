use crate::{Coord, CoordX, CoordY, Placements};

pub const NORTH: Coord = Coord(CoordX(0), CoordY(1));
pub const SOUTH: Coord = Coord(CoordX(0), CoordY(-1));
pub const NORTH_EAST: Coord = Coord(CoordX(1), CoordY(1));
pub const SOUTH_WEST: Coord = Coord(CoordX(-1), CoordY(-1));
pub const SOUTH_EAST: Coord = Coord(CoordX(1), CoordY(0));
pub const NORTH_WEST: Coord = Coord(CoordX(-1), CoordY(0));

pub fn get_valid_moves(rings: &Placements, markers: &Placements, from: Coord) -> Vec<Coord> {
    let mut result = vec![];

    let directions = [NORTH, SOUTH, NORTH_EAST, SOUTH_WEST, SOUTH_EAST, NORTH_WEST];
    for &direction in &directions {
        result.append(&mut get_valid_moves_in_direction(
            rings, markers, from, direction,
        ));
    }

    result
}

pub fn get_valid_moves_in_direction(
    rings: &Placements,
    markers: &Placements,
    from: Coord,
    direction: Coord,
) -> Vec<Coord> {
    let mut valid_coords = vec![];
    let mut has_jumped = false;

    let Coord(mut x, mut y) = from + direction;

    while x <= CoordX::MAX && x >= CoordX::MIN && y <= CoordY::MAX && y >= CoordY::MIN {
        let coord = Coord(x, y);
        if rings.get(coord).is_some() {
            break; // hit a ring. no more valid coords in this direction
        }
        let is_marker = markers.get(coord).is_some();
        if is_marker {
            has_jumped = true;
        } else {
            valid_coords.push(coord);
        }

        if !is_marker && has_jumped {
            break; // just landed after a jump. no more valid coords in this direction
        }

        let Coord(direction_x, direction_y) = direction;
        x += direction_x;
        y += direction_y;
    }

    valid_coords
}
