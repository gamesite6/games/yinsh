export const A: -11 & CoordX = -11;
export const B: -10 & CoordX = -10;
export const C: -9 & CoordX = -9;
export const D: -8 & CoordX = -8;
export const E: -7 & CoordX = -7;
export const F: -6 & CoordX = -6;
export const G: -5 & CoordX = -5;
export const H: -4 & CoordX = -4;
export const I: -3 & CoordX = -3;
export const J: -2 & CoordX = -2;
export const K: -1 & CoordX = -1;

export const xMax: CoordX = K;
export const xMin: CoordX = A;
export const yMax: CoordY = 11;
export const yMin: CoordY = 1;
