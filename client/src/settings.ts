export function getDefaultSettings(): GameSettings {
  return {
    firstToNRings: 3,
  };
}

export const validPlayerCounts = [2];

export function playerCounts(settings: GameSettings): number[] {
  return [2];
}

type NRings = 1 | 3;
let nRingsOptions: { n: NRings; description: string }[] = [
  { n: 1, description: "Short game" },
  { n: 3, description: "Standard game" },
];

export { nRingsOptions };
