import {
  A,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  xMin,
  xMax,
  yMin,
  yMax,
} from "../constants";

export function range(from: number, to: number): number[] {
  const size = Math.max(to - from, 0);
  const result = new Array(size);

  for (let i = 0; i < size; i++) {
    result[i] = from + i;
  }

  return result;
}

export function rangeInclusive(from: number, to: number): number[] {
  return range(from, to + 1);
}

export function reversed<T>(ts: T[]): T[] {
  let result = [...ts];
  result.reverse();
  return result;
}

export function indexed<T>(ts: T[]): [T, number][] {
  return ts.map((t, i) => [t, i]);
}

export function xToLetter(x: CoordX): string {
  switch (x) {
    case A:
      return "a";
    case B:
      return "b";
    case C:
      return "c";
    case D:
      return "d";
    case E:
      return "e";
    case F:
      return "f";
    case G:
      return "g";
    case H:
      return "h";
    case I:
      return "i";
    case J:
      return "j";
    case K:
      return "k";
  }
}

export function activeColor(state: GameState): Color | null {
  switch (state.phase.t) {
    case "MarkerPlacing":
    case "RingPlacing":
    case "RingMoving":
    case "MarkerTaking":
    case "RingTaking":
      return state.phase.color;
    case "GameComplete":
      return null;
  }
}

export function isStraightPath(a: Coord, b: Coord): boolean {
  const [aX, aY] = a;
  const [bX, bY] = b;

  return (
    (aX === bX && aY !== bY) ||
    (aX !== bX && aY === bY) ||
    (aX - bX === aY - bY && aX - bX !== 0)
  );
}

type Delta = [number, number];

const north: Delta = [0, 1];
const south: Delta = [0, -1];

const northEast: Delta = [1, 1];
const southWest: Delta = [-1, -1];

const northWest: Delta = [-1, 0];
const southEast: Delta = [1, 0];

export function getValidMoves(
  ctx: { rings: Placements; markers: Placements },
  from: Coord
): Coord[] {
  return [
    ...validMovesInDirection(ctx, add(from, north), north),
    ...validMovesInDirection(ctx, add(from, south), south),
    ...validMovesInDirection(ctx, add(from, northEast), northEast),
    ...validMovesInDirection(ctx, add(from, southWest), southWest),
    ...validMovesInDirection(ctx, add(from, northWest), northWest),
    ...validMovesInDirection(ctx, add(from, southEast), southEast),
  ];
}

function validMovesInDirection(
  ctx: { rings: Placements; markers: Placements },
  from: Coord,
  direction: Delta
): Coord[] {
  const validCoords: Coord[] = [];

  let hasJumped = false;

  let [x, y] = from;
  while (x <= xMax && x >= xMin && y <= yMax && y >= yMin) {
    if (ctx.rings[x]?.[y]) {
      break; // hit a ring. no more valid coords in this direction
    }

    let isMarker = !!ctx.markers[x]?.[y];
    if (isMarker) {
      hasJumped = true;
    } else {
      validCoords.push([x, y]);
    }

    if (!isMarker && hasJumped) {
      break; // just landed after a jump. no more valid coords in this direction
    }

    x += direction[0];
    y += direction[1];
  }

  return validCoords;
}

function add(coord: Coord, delta: Delta): Coord {
  return [coord[0] + delta[0], coord[1] + delta[1]] as Coord;
}

function mul(coord: Delta, n: number): Delta {
  return [coord[0] * n, coord[1] * n];
}

export function find5InARow(markers: Placements, color: Color): Coord[][] {
  const coords = getMarkerCoords(markers, color);

  let rows: Coord[][] = coords.flatMap((coord) =>
    find5InARowFromCoord(markers, color, coord)
  );

  return rows;
}

function getMarkerCoords(markers: Placements, color: Color): Coord[] {
  let coords: Coord[] = [];
  for (let [xStr, ys] of Object.entries(markers)) {
    let x = Number(xStr) as CoordX;
    if (ys) {
      for (let [yStr, markerColor] of Object.entries(ys)) {
        let y = Number(yStr) as CoordY;
        if (markerColor === color) {
          coords.push([x, y]);
        }
      }
    }
  }
  return coords;
}

function find5InARowFromCoord(
  markers: Placements,
  color: Color,
  coord: Coord
): Coord[][] {
  let [x, y] = coord;
  if (markers[x]?.[y] !== color) {
    return [];
  }

  let result = [];

  const directions = [north, northEast, southEast];
  for (let direction of directions) {
    let row = [1, 2, 3, 4].map((i) => add(coord, mul(direction, i)));
    if (row.every(([x, y]) => markers[x]?.[y] === color)) {
      row.unshift(coord);
      result.push(row);
    }
  }

  return result;
}
