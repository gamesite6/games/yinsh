import { isStraightPath } from "./utils";
import { A, B, C, D, E, F, G, H, I, J, K } from "../constants";
import { describe, test, expect } from "vitest";
describe("utils", () => {
  describe("straighPath", () => {
    test("south to north", () => {
      expect(isStraightPath([E, 1], [E, 10])).toBe(true);
    });
    test("north to south", () => {
      expect(isStraightPath([H, 9], [H, 5])).toBe(true);
    });
    test("southwest to northeast", () => {
      expect(isStraightPath([D, 3], [F, 5])).toBe(true);
    });
    test("northeast to southwest", () => {
      expect(isStraightPath([K, 7], [E, 1])).toBe(true);
    });
    test("northwest to southeast", () => {
      expect(isStraightPath([D, 8], [I, 8])).toBe(true);
    });
    test("southeast to northwest", () => {
      expect(isStraightPath([J, 10], [G, 10])).toBe(true);
    });
    test("empty path is not straight", () => {
      expect(isStraightPath([A, 3], [A, 3])).toBe(false);
    });
    test("not straight", () => {
      expect(isStraightPath([A, 3], [C, 4])).toBe(false);
      expect(isStraightPath([H, 5], [D, 6])).toBe(false);
      expect(isStraightPath([B, 1], [G, 11])).toBe(false);
    });
  });
});
