/// <reference types="svelte" />
/// <reference types="vite/client" />

type Coord = [CoordX, CoordY];
type CoordX = -11 | -10 | -9 | -8 | -7 | -6 | -5 | -4 | -3 | -2 | -1;
type CoordY = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;

type Placements = Partial<Record<CoordX, Partial<Record<CoordY, Color>>>>;

type PlayerId = number;

type Color = "b" | "w";

type GameState = {
  players: {
    black: Player;
    white: Player;
  };
  markers: Placements;
  phase: Phase;
};

type Player = {
  id: PlayerId;
  rings: PlayerRings;
};

type PlayerRings = [
  Coord | null,
  Coord | null,
  Coord | null,
  Coord | null,
  Coord | null
];

type Phase =
  | { t: "RingPlacing"; color: Color }
  | { t: "MarkerPlacing"; color: Color }
  | { t: "RingMoving"; color: Color; coord: Coord }
  | { t: "MarkerTaking"; color: Color }
  | { t: "RingTaking"; color: Color }
  | { t: "GameComplete" };

type GameSettings = {
  firstToNRings: 1 | 3;
};

type Action =
  | { t: "PlaceRing"; coord: Coord }
  | { t: "PlaceMarker"; coord: Coord }
  | { t: "MoveRing"; coord: Coord }
  | { t: "TakeMarkers"; coord: Coord }
  | { t: "TakeRing"; coord: Coord };
